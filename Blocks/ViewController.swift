//
//  ViewController.swift
//  Blocks
//
//  Created by Aleksey Knysh on 3/12/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Print hello, ios dev class
        let hello = "Hello, ios dev class"
        let printHello = { print($0) }
        printHello(hello)
        
    }
}
